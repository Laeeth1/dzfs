module dzfs.lzc;

import std.stdio;
import pyd.pyd, pyd.embedded;

shared static this() {
    py_init();
}

/++
Determine if dataset or pool exists
 
Params:
s = name of dataset/pool to check
 +/
public void lzc_exists(string s) {
    string stmt = format("import libzfs_core\nif libzfs_core.lzc_exists('%s'):\n\tprint('yes')", s);
    py_stmts(stmt);
}

/// Doc
public void ping1() {
	writeln("pong1");
}